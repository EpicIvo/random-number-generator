import styled from 'styled-components';

export const Container = styled.div`
  background: rgba(0, 0, 0, 0.6);
  position: fixed;
  width: 100%;
  height: 100vh;
  top: 0;
`;
