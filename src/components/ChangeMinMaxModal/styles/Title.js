import styled from 'styled-components';

export const Title = styled.h3`
  font-family: "Proxima Nova", sans-serif;
  font-size: 40px;
  text-transform: capitalize;
  margin: 25px 0 0 0;
  text-align: center;
`;
