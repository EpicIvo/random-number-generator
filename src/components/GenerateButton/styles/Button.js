import styled from 'styled-components';

export const Button = styled.button`
  font-family: "Proxima Nova", sans-serif;
  font-size: 30px;
  margin: 0 0 0 0;
  padding: 15px 25px;
  color: #ffffff;
  background: #10A8E5;
  border-radius: 34px;
  border: none;
  box-shadow: rgba(76, 80, 95, 0.21) 0 4px 5px;
`;
