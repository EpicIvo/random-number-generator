import styled from 'styled-components';

export const HistoryItems = styled.div`
  margin: -40px 30px 10px 30px;
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  z-index: -1;
  min-height: 130px;
`;
