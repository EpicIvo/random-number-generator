import styled from 'styled-components';

export const Title = styled.h2`
  font-family: "Proxima Nova", sans-serif;
  font-size: 30px;
  color: #E9EBF0;
  justify-self: center;
  z-index: -2;
`;
