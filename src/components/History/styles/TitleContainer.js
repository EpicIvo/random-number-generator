import styled from 'styled-components';

export const TitleContainer = styled.div`
	margin: 10px 0 0 0;
	display: grid;
	grid-template-columns: 1fr 1fr;
`;
