import styled from 'styled-components';

export const Backdrop = styled.h2`
	font-family: "Proxima Nova", sans-serif;
	font-size: 60px;
	margin: 0;
	color: #E9EBF0;
`;
