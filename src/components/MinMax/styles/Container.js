import styled from 'styled-components';

export const Container = styled.h2`
	margin: 50px 0 0 0;
	display: grid;
	grid-template-columns: 1fr 1fr;
`;
