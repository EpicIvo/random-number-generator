import styled from 'styled-components';

export const Number = styled.div`
  font-family: "Proxima Nova", sans-serif;
	font-size: 60px;
	margin: -40px 0 0 0;
	color: #4C505F;
`;
