import styled from 'styled-components';

export const Number = styled.h2`
	font-family: "Proxima Nova", sans-serif;
	font-size: 100px;
	margin: -50px 0 0 0;
	color: #4C505F;
`;
