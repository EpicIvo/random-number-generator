import styled from 'styled-components';

export const Title = styled.h1`
  font-family: "Proxima Nova", sans-serif;
  font-size: 20px;
  text-align: center;
  margin: 10px 0 0 0;
  color: #10A8E5;
`;
